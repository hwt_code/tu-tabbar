const activeSpan = document.querySelector('span.wrap');
const lis = document.querySelectorAll('li')
const spanWidth = activeSpan.getBoundingClientRect().width.toFixed(2)
const liWidth = lis[0].getBoundingClientRect().width.toFixed(2)
lis.forEach((it, idx) => {
  it.addEventListener('click', () => {
    // 更换active态图标
    activeSpan.innerHTML = ''
    const span = document.createElement('span')
    span.classList.add('active')
    const icon = it.querySelector('i').cloneNode(true)
    span.appendChild(icon)
    activeSpan.appendChild(span)
    // 计算activeSpan的x距离
    const gap = (liWidth - spanWidth) / 2
    const x = `${idx * liWidth + gap}px`
    activeSpan.style.setProperty('transform', `translateX(${x})`)
  })
})
